<?php

namespace App\Providers;

use App\Models\Language;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       // Model::preventLazyLoading(! $this->app->isProduction());

        $data = [];
        if(Schema::hasTable('languages')){
            $languages = Language::all();

            foreach ($languages as $language){
                $data[$language->localization]  = $language->title;
            }

            if(!empty($data)){
                config(['nova-translatable.locales' => $data]);
                config(['translatable.locales' => array_keys($data)]);
            }
        }
    }
}
