<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ServiceTranslation extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['title','subtitle','description'];

    protected static function boot()
    {
        parent::boot();

        static::saving(function($model){
            $model->slug = Str::slug($model->title);
        });
    }
}
