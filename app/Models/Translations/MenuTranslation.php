<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class MenuTranslation extends Model
{
    use HasFactory;
    protected $fillable = ['title'];
    public $timestamps = false;

    protected static function boot()
    {
        parent::boot();
        static::saving(function($model){
            $model->slug = Str::slug($model->title);
        });
    }
}
