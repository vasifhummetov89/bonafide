<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Spatie\EloquentSortable\SortableTrait;

class Service extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatedAttributes = ['description','title','subtitle','slug'];
    protected $fillable = ['image'];

}
