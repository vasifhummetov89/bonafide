<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Support\Str;
use Spatie\EloquentSortable\SortableTrait;


class Team extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;
    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public $translatedAttributes = ['description','full_name'];
    protected $fillable = ['position_id','image','email','phone','languages'];

    protected $casts = [
        'languages' => 'array'
    ];

    public function position(){
        return $this->belongsTo(Position::class);
    }

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model){
            $model->team_languages()->detach($model->languages);
            $model->team_languages()->sync($model->languages);
        });

        static::saving(function ($model){
            $model->slug = Str::slug($model->full_name);
        });
    }

    public function team_languages(){
        return $this->belongsToMany(Language::class,'team_languages');
    }
}
