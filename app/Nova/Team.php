<?php

namespace App\Nova;

use Ek0519\Quilljs\Quilljs;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\File;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use OptimistDigital\MultiselectField\Multiselect;
use OptimistDigital\NovaSortable\Traits\HasSortableRows;

class Team extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */

    use HasSortableRows;

    public static $model = \App\Models\Team::class;

    public $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
        'nova_order_by' => 'DESC',
    ];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            File::make('Image')->creationRules(['required','mimes:jpeg,jpg,png'])
                ->updateRules(['mimes:jpeg,jpg,png']),
            Text::make('Full Name')->rules(['required'])->translatable(),
            Text::make('Email')->rules(['required','email']),
            Text::make('Phone')->rules(['required']),
            BelongsTo::make('Position'),
            Multiselect::make('Languages')
                ->options($this->languages())
                ->rules(['required'])
                ->saveAsJSON()
                ->reorderable(),
            Quilljs::make('Description')
                ->rules(['required'])
                ->withFiles('public')
                ->uploadUrlSplit('.')
                ->translatable()
        ];
    }

    protected function languages(){
        $data = [];
        $languages = \App\Models\Language::all();
        foreach ($languages as $language){
            $data[$language->id] = $language->title;
        }

        return $data;
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
