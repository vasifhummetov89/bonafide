<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Menu;
use App\Models\Partner;
use App\Models\Service;
use App\Models\Slider;
use App\Models\Team;

class HomeController extends Controller
{
    public function index()
    {

        $about = About::latest()->first();
        $sliders = Slider::all();
        $services = Service::all();
        $partners = Partner::all();

        return redirect('/nova');
    }

    public function service($slug)
    {
        $service = Service::whereHas('translation', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->firstOrFail();

        return [];
    }


    public function teams()
    {
        $teams = Team::latest()->get();

        return [];
    }

    public function team($slug)
    {
        $team = Team::whereRelation('translation', 'slug', $slug)->firstOrFail();

        return [];
    }

    public function news()
    {

    }

    public function singleNew($slug)
    {

    }


    public function page($slug)
    {
        $menu = Menu::whereRelation('translation', 'slug', $slug)->firstOrFail()->pages;
        return $menu;
    }

}
