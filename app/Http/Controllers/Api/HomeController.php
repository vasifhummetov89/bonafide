<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\About\AboutResource;
use App\Http\Resources\Certificate\CertificateCollection;
use App\Http\Resources\Language\LanguageCollection;
use App\Http\Resources\Menu\MenuCollection;
use App\Http\Resources\Menu\MenuResource;
use App\Http\Resources\Page\PageCollection;
use App\Http\Resources\Page\PageResource;
use App\Http\Resources\Partner\PartnerCollection;
use App\Http\Resources\Position\PositionCollection;
use App\Http\Resources\Position\PositionResource;
use App\Http\Resources\Service\ServiceCollection;
use App\Http\Resources\Service\ServiceResource;
use App\Http\Resources\Setting\SettingResource;
use App\Http\Resources\Slider\SliderCollection;
use App\Http\Resources\Team\TeamCollection;
use App\Http\Resources\Team\TeamResource;
use App\Http\Resources\TeamLanguage\TeamLanguageCollection;
use App\Http\Resources\TranslationCollection;
use App\Models\About;
use App\Models\Certificate;
use App\Models\Language;
use App\Models\Menu;
use App\Models\Partner;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Team;
use App\Models\Translation;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

        return [
            'about'     => $this->about(),
            'sliders'   => $this->sliders(),
            'services'  => $this->homePageServices(),
            'partners'  => $this->partners()
        ];
    }

    public function sliders(){
        $sliders = Slider::all();
        return new SliderCollection($sliders);
    }

    public function about(){
        $about = About::latest()->first();
        return $about ? new AboutResource($about) : ['description' => null];
    }

    public function homePageServices(){
        $services = Service::limit(6)->ordered()->get();
        return new ServiceCollection($services);
    }

    public function certificates(){
        $certificates = Certificate::all();
        return new CertificateCollection($certificates);
    }

    public function languages(){
        $languages = Language::where('active',1)->get();
        return new LanguageCollection($languages);
    }

    public function services(){
        return new ServiceCollection(Service::ordered()->get());
    }

    public function service($slug){

        $service = Service::whereHas('translation',function($query) use ($slug){
            $query->where('slug',$slug);
        })->firstOrFail();
        return $service ?  new ServiceResource($service) : ['data' => []];
    }

    public function partners(){
        $partners = Partner::ordered()->get();
        return new PartnerCollection($partners);
    }

    public function menus(){
        $menus = Menu::all();
        return new MenuCollection($menus);
    }


    public function search(Request $request){
        $q = $request->get('query');

        $services = Service::query();

        $services = $services->when($request->has('query'),function ($query) use($q){
            $query->whereHas('translation',function ($query) use($q){
                $query->where('title','like','%'.$q.'%');
            });
        })->get();

       return new ServiceCollection($services);
    }

    public function teams(){
        $teams = Team::ordered()->get();
        $teams = $teams->chunk(4);
        $teams = $teams->all();

        return new TeamCollection($teams);
    }

    public function team($slug){
        $team = Team::where('slug',$slug)->firstOrFail();
        return [
            'id'          => $team->id,
            'position'    => new PositionResource($team->position),
            'fullName'    => $team->full_name,
            'image'       => asset('storage/'.$team->image),
            'email'       => $team->email,
            'phone'       => $team->phone,
            'description' => $team->description,
            'slug'        => $team->slug,
            'languages'   => new TeamLanguageCollection($team->team_languages)
        ];
    }

    public function page($slug){
        $menu = Menu::whereRelation('translation','slug',$slug)->firstOrFail();
        return (new PageCollection($menu->pages))->additional([
            'menu' => new MenuResource($menu)
        ]);
    }

    public function pageSingle($menu,$slug){

        $menu = Menu::whereRelation('translation','slug',$menu)->firstOrFail();

        $page = $menu->pages()->whereRelation('translation','slug',$slug)
            ->firstOrFail();

        $pages = $menu->pages()->whereHas('translation',function ($query) use ($slug){
            $query->where('slug','!=',$slug);
        })->get();

        return (new PageResource($page))->additional([
            'pages' => new PageCollection($pages),
            'menu'  => new MenuResource($menu)
        ]);
    }

    public function setting(){
        $setting = Setting::latest()->first();
        return new SettingResource(optional($setting));
    }

    public function translation($locale){

        app()->setLocale($locale);
        $translations = Translation::all();

        $data = [];

        foreach ($translations as $translation){
            $data[$translation->key] = $translation->value;
        }

        return $data;
    }

}
