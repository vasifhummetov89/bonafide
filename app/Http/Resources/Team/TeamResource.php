<?php

namespace App\Http\Resources\Team;

use App\Http\Resources\Position\PositionCollection;
use App\Http\Resources\Position\PositionResource;
use App\Http\Resources\TeamLanguage\TeamLanguageCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $data = [];

        foreach ($this as $teams) {
            foreach ($teams as $team) {
                $data[] = [
                    'id'          => $team->id,
                    'position'    => new PositionResource($team->position),
                    'fullName'    => $team->full_name,
                    'image'       => asset('storage/'.$team->image),
                    'email'       => $team->email,
                    'phone'       => $team->phone,
                    'description' => $team->description,
                    'slug'        => $team->slug,
                    'languages'   => new TeamLanguageCollection($team->team_languages)
                ];
            }
        }

        return $data;
    }
}
