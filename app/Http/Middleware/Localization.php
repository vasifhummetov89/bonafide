<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->hasHeader('Localization')){
            app()->setLocale($request->header('Localization'));
        }else{
            app()->setLocale(config('app.locale'));
        }

        return $next($request);
    }
}
