<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('languages',[\App\Http\Controllers\Api\HomeController::class,'languages']);

Route::group(['middleware' => 'Localization'],function (){

    Route::get('init',[\App\Http\Controllers\Api\HomeController::class,'index']);

    Route::get('certificates',[\App\Http\Controllers\Api\HomeController::class,'certificates']);

    Route::get('menus',[\App\Http\Controllers\Api\HomeController::class,'menus']);

    Route::get('menu/{slug}',[\App\Http\Controllers\Api\HomeController::class,'menu']);

    Route::get('services',[\App\Http\Controllers\Api\HomeController::class,'services']);

    Route::get('service/{slug}',[\App\Http\Controllers\Api\HomeController::class,'service']);

    Route::get('search',[\App\Http\Controllers\Api\HomeController::class,'search'])->name('search');

    Route::get('teams',[\App\Http\Controllers\Api\HomeController::class,'teams']);

    Route::get('team/{slug}',[\App\Http\Controllers\Api\HomeController::class,'team']);

    Route::get('page/{menu}/{slug}',[\App\Http\Controllers\Api\HomeController::class,'pageSingle']);

    Route::get('page/{menu}',[\App\Http\Controllers\Api\HomeController::class,'page']);

    Route::get('setting',[\App\Http\Controllers\Api\HomeController::class,'setting']);

    Route::get('translation/{locale}',[\App\Http\Controllers\Api\HomeController::class,'translation']);

});
